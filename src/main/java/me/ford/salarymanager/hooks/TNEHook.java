package me.ford.salarymanager.hooks;

import java.math.BigDecimal;

import me.ford.salarymanager.bank.BankAccount;
import net.tnemc.core.TNE;
import net.tnemc.core.common.api.TNEAPI;
import net.tnemc.core.economy.Account;

public class TNEHook {
    private final TNEAPI api;

    public TNEHook() {
        this.api = TNE.instance().api();
    }

    public TNEAPI getAPI() {
        return api;
    }

    public BankAccount createBank(String id) {
        TNEBank bank = new TNEBank(id);
        bank.getAccount(); // try to get account
        return bank;
    }

    private class TNEBank implements BankAccount {
        private final String identifier;

        private TNEBank(String identifier) {
            this.identifier = identifier;
        }

        public Account getAccount() {
            return api.getAccount(identifier);
        }

        @Override
        public double getMoney() {
            return getAccount().getHoldings().doubleValue();
        }

        @Override
        public void removeMoney(double amount) throws IllegalArgumentException {
            if (amount < 0) {
                throw new IllegalArgumentException("Cannot remove negative value");
            }
            getAccount().removeHoldings(new BigDecimal(amount));
        }

        @Override
        public void addMoney(double amount) throws IllegalArgumentException {
            if (amount < 0) {
                throw new IllegalArgumentException("Cannot add negative value");
            }
            getAccount().addHoldings(new BigDecimal(amount));
        }

        @Override
        public String asSaveableString() {
            return "TNE:" + identifier;
        }

    }

}
